#OnePage

An example one-page.

![image](https://i.imgur.com/86xD3Id.jpg)


### Things I used
* [initializr](http://www.initializr.com/) - Bootstrap (v3), error page, robots, ...
* [CodePen](https://codepen.io/AldoF/pen/MGoawJ) - Step by step implementation
* [Bootstrap 4](https://getbootstrap.com/) - For mobile-first development
